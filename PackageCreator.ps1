﻿

$workingDir = "E:\Interaction_Dialer"
if(!(Test-Path "$env:USERPROFILE\.git-credentials")){
    $cred = Get-Credential
    git config --global credential.helper store
    Add-Content "$env:USERPROFILE\.git-credentials" "https://$($cred.UserName):$($cred.GetNetworkCredential().password)@bitbucket.org`n"
}

git remote set-url origin3 https://ininpsoappveyor@bitbucket.org/wjdavis5/dialernugetcreator.git
git remote -v

git pull origin2 master --quiet

function fCopyFiles {param($mount,$workingDir)
    $volume = Get-DiskImage -ImagePath $mount.ImagePath | Get-Volume
    $source = $volume.DriveLetter + ":\"
   
     Get-ChildItem -Path $source -Recurse -File "DialerDeveloper*.ms*" | ForEach-Object -Process {
        "Copying $_.Fullname"
        $test = $workingDir + "\" + $_.Name
        if(!(Test-Path $test)){
            Copy-Item -Path $_.FullName -Destination $workingDir
            "Copied $_.Fullname"
        }
    }

 
}

Get-ChildItem -Path "x:\" -Recurse -File -Filter "Dialer_2015*.iso" | ForEach-Object -Process {
    $mount_params = @{ImagePath = $_.FullName; PassThru = $true; ErrorAction = "Ignore"}
    $mount_params
    $mount = Mount-DiskImage @mount_params
    if($mount){
        fCopyFiles $mount $workingDir
        "Dismounting $_.Fullname"
        $hide = Dismount-DiskImage -ImagePath $mount_params.ImagePath
        "Dismounted $_.Fullname"
    }
    else{
        #apparently the mount operation failed. lets copy it local and try again.
        $newPath = $workingDir + "\" + $_.Name
        if(!(Test-Path $newPath)){
            Copy-Item -Path $_.FullName -Destination $workingDir
        }
        
        $newPath = $workingDir + "\" + $_.Name
        $mount_params = @{ImagePath = $newPath; PassThru = $true; ErrorAction = "Ignore"; StorageType = "ISO"}
        $mount_params
        $mount = Mount-DiskImage @mount_params
        if($mount){
            fCopyFiles $mount $workingDir
            "Dismounting $_.Fullname"
            $hide = Dismount-DiskImage -ImagePath $mount_params.ImagePath
            "Dismounted $_.Fullname"
        }
    }


}
