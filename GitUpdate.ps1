﻿if(!(Test-Path "$env:USERPROFILE\.git-credentials")){
    $cred = Get-Credential
    git config --global credential.helper store
    Add-Content "$env:USERPROFILE\.git-credentials" "https://$($cred.UserName):$($cred.GetNetworkCredential().password)@bitbucket.org`n"
}

git remote set-url origin3 https://ininpsoappveyor@bitbucket.org/wjdavis5/dialernugetcreator.git
git remote -v

git pull origin2 master --quiet