﻿#msiexec /a $_.FullName /qb TARGETDIR="$extractDir" /update DialerDeveloper_2015_R1_Patch17.msp

$workingDir = "E:\Interaction_Dialer"

Get-ChildItem -Directory -Filter R* -Path $workingDir | ForEach-Object -Process {
    $newWorkingDir = $_.FullName
    $msiextractDir = $newWorkingDir + "\extract"
    $baseBinPath = "\Interactive Intelligence\Dialer Developer\bin"
    $postExtractDir = $msiextractDir + $baseBinPath
    
    Get-ChildItem -File -Filter *.msi -Path $newWorkingDir | ForEach-Object -Process{
            $msiFullPath = $_.FullName
        $msiBackupPath = $_.FullName + ".bak"
        $msiBackupPath
        $msiFullPath
        "Backing up MSI File"
        Copy-Item -Path $msiFullPath -Destination $msiBackupPath
        "Backed up file"
       
        $argList = "/a $($_.FullName) /qn TARGETDIR=$msiextractDir"
        $argList
        Start-Process -FilePath msiexec -ArgumentList $argList -PassThru -Wait
        
        .\CreateNuPkg.ps1 "Dialer.nuspec" $postExtractDir $workingDir
        
        Get-ChildItem -File "*.msp" -Path $newWorkingDir | ForEach-Object -Process {
             "Removing Msi File"
             Remove-Item -Path $msiFullPath -Force
             "Removed MSI File"
            

             "Restoring MSI From Backup"
             Copy-Item -Path $msiBackupPath -Destination $msiFullPath
             "Restored from backup"
          
             $mspextractDir = $msiextractDir + "\" + $_.Name.Split("_")[3].Replace(".msp","")
        
            $argList = "/a $($msiFullPath) /qn TARGETDIR=$mspextractDir /update $($_.FullName)"
            $argList
        
            Start-Process -FilePath msiexec -ArgumentList $argList  -PassThru -Wait

            $extractedBinPath = $mspextractDir + $baseBinPath
           .\CreateNuPkg.ps1 "Dialer.nuspec" $extractedBinPath $workingDir
        
        }
    }
}