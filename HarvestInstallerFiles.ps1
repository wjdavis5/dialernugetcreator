﻿

$workingDir = "E:\Interaction_Dialer"

function fCopyFiles {param($mount,$workingDir)
    $volume = Get-DiskImage -ImagePath $mount.ImagePath | Get-Volume
    $source = $volume.DriveLetter + ":\"
     Get-ChildItem -Path $source -Recurse -File "DialerDeveloper*.ms*" | ForEach-Object -Process {
        $targetDir = $workingDir + "\" + $_.Name.Split("_")[2].Split(".")[0]
        $test = $targetDir + "\" + $_.Name
        if(!(Test-Path -Path $targetDir )){
            New-Item -ItemType directory -Path $targetDir
        }
        if(!(Test-Path $test)){
            "Copying $_."
            Copy-Item -Path $_.FullName -Destination $targetDir
            $finalFullPath = $targetDir + "\" + $_.Name
            sp $finalFullPath IsReadOnly $false
            "Copied $_."
        }
    }

 
}

Get-ChildItem -Path "x:\" -Recurse -File -Filter "Dialer_2015*.iso" | ForEach-Object -Process {
    $mount_params = @{ImagePath = $_.FullName; PassThru = $true; ErrorAction = "Ignore"}
    $mount = Mount-DiskImage @mount_params
    if($mount){
        fCopyFiles $mount $workingDir
        "Dismounting $_."
        $hide = Dismount-DiskImage -ImagePath $mount_params.ImagePath
        "Dismounted $_."
    }
    else{
        #apparently the mount operation failed. lets copy it local and try again.
        $newPath = $workingDir + "\" + $_.Name
        if(!(Test-Path $newPath)){
            Copy-Item -Path $_.FullName -Destination $workingDir
        }
        
        $newPath = $workingDir + "\" + $_.Name
        $mount_params = @{ImagePath = $newPath; PassThru = $true; ErrorAction = "Ignore"; StorageType = "ISO"}
        $mount = Mount-DiskImage @mount_params
        if($mount){
            fCopyFiles $mount $workingDir
            "Dismounting $_."
            $hide = Dismount-DiskImage -ImagePath $mount_params.ImagePath
            "Dismounted $_."
        }
    }
}

Get-ChildItem -Path "E:\Interaction_Dialer" -File "*.ms*" | ForEach-Object -Process { 
    $targetDir = $workingDir + "\" + $_.Name.Split("_")[2].Split(".")[0]
    
}
