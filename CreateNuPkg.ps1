﻿param(
    [string]$nuspecPath,
    [string]$filePath,
    [string]$workingPath
)
#$nuspecPath = "C:\Users\william.davis\Documents\git\NugetPackageCreator\Dialer.nuspec"
#$filepath = "E:\Interaction_Dialer\R1\extract\Interactive Intelligence\Dialer Developer\bin"
#$workingPath = "E:\Interaction_Dialer"
$libPath = $filepath + "\lib\net20"
$nuspecBackupPath = $nuspecPath + ".bak"
$outputPath = $workingPath + "\Nuget"
$fileName = $filepath + "\ININ.IceLib.Dialer.dll"
$fileVersion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($fileName).FileVersion
"Restoring Nuspec File"
Copy-Item -Path $nuspecBackupPath -Destination $nuspecPath
"Backed up file"
"Updating Nuspec version info"
$fileVersion2 = $fileVersion.Split(".")[0] + "." + $fileVersion.Split(".")[1] + "." + $fileVersion.Split(".")[2]
(gc $nuspecPath).replace('##VERSION##',$fileVersion)|sc $nuspecPath
(gc $nuspecPath).replace('##VERSION2##',$fileVersion2)|sc $nuspecPath
"Set Fileversion to $fileVersion"

Get-ChildItem -File -Path $filepath | ForEach-Object -Process {
    if(($_.Name -eq "ININ.IceLib.Dialer.dll") -Or ($_.Name -eq "ININ.DataStreams.dll")){
       
          if(!(Test-Path -Path $libPath )){
            New-Item -ItemType directory -Path $libPath
        }
        Copy-Item -Path $_.FullName -Destination $libPath
        Remove-Item -Path $_.FullName -Force
    }
    else
    {
       Remove-Item -Path $_.FullName -Force
    }
}
Copy-Item -Path $nuspecPath -Destination $filepath
$finalNuspecPath = $filepath + "\Dialer.nuspec"
$argList = " pack '$($finalNuspecPath)'"
$argList
nuget pack $finalNuspecPath -OutputDirectory $filepath
.\UploadPackage.ps1 $filepath